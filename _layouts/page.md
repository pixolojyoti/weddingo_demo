---
testvar: "Pixolo Test Var"
---

<html lang="en">

{% include header.html %}

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    
    {% if page.navneeded == true %}
        <header class="header-area">
            {% include navbar.html %}
        </header>
    {% endif %}

<div style="margin-top: 150px">

    <h1>{{ page.pagetitle }}</h1>

    <p>{{ layout.testvar }}</p>
    
    {{ content }}

</div>

{% include footer.html %}

</body>
</html>