---
layout: page
title: About
navneeded: true
pagetitle: "Pixolo About Page"
permalink: /about/
---

THIS IS ABOUT US CONTENT

{{ site.data.aboutdata | inspect }}


{% for person in site.data.aboutdata %}

{{ forloop.index0  }}

{{ forloop.index0 | to_integer }}

{% if forloop.index0 >= 1 %}
{{ person | inspect }}
<div style="display: flex;align-items: center; justify-content: space-between;width: 50%; margin: auto;">
    <p style="font-size: 2em; font-family: sans serif; color: red;">
        {{ person.rollnumber }}
    </p>
    <p style="font-size: 1em; font-family: sans serif; color: red;">
        {{ person.name }}
    </p>
    {{ person.image }}
</div>
{% endif %}
{% endfor %}